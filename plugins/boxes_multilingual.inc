<?php

/**
 * Simple custom text box.
 */
class boxes_multilingual extends boxes_box {
  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
    $languages = language_list();

    foreach($languages as $lang){
      $defaults[$lang->language]['title'] = '';
      $defaults[$lang->language]['body'] = array(
          'value' => '',
          'format' => filter_default_format(),
      );
    }
    return $defaults;

  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form(&$form_state) {
    global $language;

    $form = array();

    $form['tabs'] = array(
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-' . $language->language,
      );

    $languages = language_list();
    foreach($languages as $lang){
      $defaults = isset($this->options[$lang->language]) ? $this->options[$lang->language] : array();
      $form[$lang->language] = array(
        '#type' => 'fieldset',
        '#title' => $lang->native,
        '#collapsible' => TRUE,
        '#tree' => TRUE,
        '#group' => 'tabs',
        );
      $form[$lang->language]['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#default_value' => isset($defaults['title']) ? $defaults['title'] : '',
      );
      $form[$lang->language]['body'] = array(
        '#type' => 'text_format',
        '#base_type' => 'textarea',
        '#title' => t('Content'),
        '#default_value' => isset($defaults['body']['value']) ? $defaults['body']['value'] : '',
        '#rows' => 6,
        '#format' => isset($defaults['body']['format']) ? $defaults['body']['format'] : NULL,
      );
    }

    $form['#box_type'] = 'boxes_multilingual';

    return $form;
  }

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    global $language;

    if (isset($this->options[$language->language])) {
      $values = $this->options[$language->language];

      if (isset($values['body']['format'])) {
        $content = check_markup($values['body']['value'], $values['body']['format'], $language->language, FALSE);
      }
      else {
        $content = $values['body']['value'];
      }

      $title = $values['title'];
    }
    else {
      $title = $content = '';
    }

    return array(
      'delta' => $this->delta, // Crucial.
      'title' => $title,
      'subject' => $title,
      'content' => $content,
    );
  }
}
